import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioBotonesComponent } from './formulario-botones.component';

describe('FormularioBotonesComponent', () => {
  let component: FormularioBotonesComponent;
  let fixture: ComponentFixture<FormularioBotonesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioBotonesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioBotonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
