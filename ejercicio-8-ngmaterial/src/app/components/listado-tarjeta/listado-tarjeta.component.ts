import { Component, OnInit } from '@angular/core';
import { TarjetaCredito } from 'src/app/modelos/tarjetacredito';
import { DatosUsuario } from 'src/app/interfaces/tarjeta.interface';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { TarjetaService } from 'src/app/servicios/tajeta.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { from } from 'rxjs';

@Component({
  selector: 'app-listado-tarjeta',
  templateUrl: './listado-tarjeta.component.html',
  styleUrls: ['./listado-tarjeta.component.css']
})
export class ListadoTarjetaComponent implements OnInit {

  listUsuarios: DatosUsuario[] = [];
  displayedColumns: string[] = ['titular', 'numeroTarjeta', 'fechaExpiracion', 'accion'];
  dataSource !: MatTableDataSource<any>;


  constructor(private _usuarioService: TarjetaService, private _snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.cargarUsuario();
  }
  cargarUsuario() {
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
  }
  eliminarUsuario(index: number) {
    console.log(index);

    const alerta = confirm("Estas seguro de eliminar este usuario");
    if (alerta) {
      this._usuarioService.eliminarUsuario(index);
      this.cargarUsuario();
      this._snackbar.open('El Usuario fue eliminado con exito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
    }
  }
}
